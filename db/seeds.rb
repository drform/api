# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

1.times do	
	DataType.create([
		{
			type_value: "Date",
			ref_index: "1",
		},
		{
			type_value: "Datetime",
			ref_index: "2",
		},
		{
			type_value: "String",
			ref_index: "3",
		},
		{
			type_value: "Text",
			ref_index: "4",
		},
		{
			type_value: "Integer",
			ref_index: "5",
		},
		{
			type_value: "Float",
			ref_index: "6",
		},
		{
			type_value: "Checkbox",
			ref_index: "7",
		},
		{
			type_value: "File",
			ref_index: "8",
		},
		{
			type_value: "Dropdown",
			ref_index: "9",
		},
		{
			type_value: "Image",
			ref_index: "10",
		}
	])
	Role.create([
		{
			role: "Admin"
		},
		{
			role: "User"
		},
		{
			role: "User Admin"
		}	
	])	
	ShareRole.create([
		{
			role: "Owner"
		},
		{
			role: "Other"
		},
		{
			role: "Admin"
		}
	])
	Permission.create([
		{
			role_id: "1",
			permission_description: "User: Create Read Update Delete,  Form: Read, UserForm: Read"
		},
		{
			role_id: "2",
			permission_description: "User: Create Read Update,  Form: Create Read Update Delete, UserForm: Create Read Update Delete"
		},
		{
			role_id: "3",
			permission_description: "User: Create Read Update Delete,  Form: Create Read Update Delete, UserForm: Create Read Update Delete"
		}
	])	
	ShareRolePermission.create([
		{
			share_role_id: "1",
			permission_description: "Form: Create Read Update Delete, UserForm: Create Read Update Delete"
		},
		{
			share_role_id: "2",
			permission_description: "Form: Read, UserForm: Create Read Update Delete"
		},
		{
			share_role_id: "3",
			permission_description: "Form: Create Read Update Delete, UserForm: Create Read Update Delete"
		}
	])	

	User.create([{
		username: "admin".downcase,
		password: Digest::SHA2.new(512).hexdigest("12345678"),
		name: "Admin",
		email: "momeform100@gmail.com",
		tel: "0812345678",
		position: "Administator",
		role_id: 1,		
	},{
		username: ("M.Yongpisanpop").downcase,
		password: Digest::SHA2.new(512).hexdigest("12345678"),
		name: "M.Yongpisanpop",
		email: "example1@gmail.com",
		tel: "0812345671",
		position: "--",
		role_id: 3,	
	},{
		username: ("T.Warasit").downcase,
		password: Digest::SHA2.new(512).hexdigest("12345678"),
		name: "T.Warasit",
		email: "example2@gmail.com",
		tel: "0812345672",
		position: "--",
		role_id: 2,	
	},{
		username: ("R.Pipatnanun").downcase,
		password: Digest::SHA2.new(512).hexdigest("12345678"),
		name: "R.Pipatnanun",
		email: "example3@gmail.com",
		tel: "0812345673",
		position: "--",
		role_id: 2,		
	},{
		username: ("T.Pusodsri").downcase,
		password: Digest::SHA2.new(512).hexdigest("12345678"),
		name: "T.Pusodsri",
		email: "example4@gmail.com",
		tel: "0812345674",
		position: "--",
		role_id: 2,		
	},{
		username: ("A.Prasitchai").downcase,
		password: Digest::SHA2.new(512).hexdigest("12345678"),
		name: "A.Prasitchai",
		email: "example5@gmail.com",
		tel: "0812345675",
		position: "--",
		role_id: 2,		
	},{
		username: ("tester01").downcase,
		password: Digest::SHA2.new(512).hexdigest("12345678"),
		name: "Tester01",
		email: "example6@gmail.com",
		tel: "0812345676",
		position: "--",
		role_id: 3,		
	},{
		username: ("tester02").downcase,
		password: Digest::SHA2.new(512).hexdigest("12345678"),
		name: "Tester02",
		email: "example7@gmail.com",
		tel: "0812345677",
		position: "--",
		role_id: 2,		
	},{
		username: ("tester03").downcase,
		password: Digest::SHA2.new(512).hexdigest("12345678"),
		name: "Tester03",
		email: "example8@gmail.com",
		tel: "0812345678",
		position: "--",
		role_id: 2,		
	},{
		username: ("tester04").downcase,
		password: Digest::SHA2.new(512).hexdigest("12345678"),
		name: "Tester04",
		email: "example9@gmail.com",
		tel: "0812345679",
		position: "--",
		role_id: 3,		
	}])

	admin = User.find(1)
	admin.image.attach(io:File.open(Rails.root.join("./doctor.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
	first = User.find(2)
	first.image.attach(io:File.open(Rails.root.join("./doctor.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
	second = User.find(3)
	second.image.attach(io:File.open(Rails.root.join("./doctor.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))	
	third = User.find(4)
	third.image.attach(io:File.open(Rails.root.join("./doctor.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
	fourth = User.find(5)
	fourth.image.attach(io:File.open(Rails.root.join("./doctor.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
	fift = User.find(6)
	fift.image.attach(io:File.open(Rails.root.join("./doctor.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
	six = User.find(7)
	six.image.attach(io:File.open(Rails.root.join("./doctor.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
	seven = User.find(8)
	seven.image.attach(io:File.open(Rails.root.join("./doctor.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
	eight = User.find(9)
	eight.image.attach(io:File.open(Rails.root.join("./doctor.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))

	Group.create([{
		user_id: 2,
		title: "Group",
		description: "first group"
	},{
		user_id: 6,
		title: "Riseplus Group",
		description: "for test"
	}])

	group = Group.find(1)
	group.image.attach(io:File.open(Rails.root.join("./folder.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
	group1 = Group.find(2)
	group1.image.attach(io:File.open(Rails.root.join("./folder.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
end