class CreateUserFormItems < ActiveRecord::Migration[5.2]
  def change
    create_table :user_form_items do |t|
      t.integer :user_form_id
      t.integer :form_item_id
      t.date :date_value, default: nil
      t.datetime :datetime_value, default: nil
      t.string :string_value, default: nil
      t.text :text_value, default: nil
      t.integer :integer_value, default: nil
      t.float :float_value, default: nil
      t.integer :checkbox_value, default: nil
      t.string :file_value, default: nil
      t.integer :dropdown_value_id, default: nil
      t.string :image_value, default: nil
      t.boolean :boolean_value, default: nil

      t.timestamps
    end
  end
end
