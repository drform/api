class CreateFormItems < ActiveRecord::Migration[5.2]
  def change
    create_table :form_items do |t|
      t.integer :form_id
      t.integer :order_no
      t.integer :data_type_id
      t.string :title
      t.string :description
      t.boolean :required
      t.boolean :av_search

      t.timestamps
    end
  end
end
