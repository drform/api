class CreateForms < ActiveRecord::Migration[5.2]
  def change
    create_table :forms do |t|
      t.integer :user_id
      t.integer :group_id
      t.string :title
      t.string :description

      t.timestamps
    end
  end
end
