class CreatePermissions < ActiveRecord::Migration[5.2]
  def change
    create_table :permissions do |t|
      t.integer :role_id
      t.string :permission_description

      t.timestamps
    end
  end
end
