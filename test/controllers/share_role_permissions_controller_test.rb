require 'test_helper'

class ShareRolePermissionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @share_role_permission = share_role_permissions(:one)
  end

  test "should get index" do
    get share_role_permissions_url, as: :json
    assert_response :success
  end

  test "should create share_role_permission" do
    assert_difference('ShareRolePermission.count') do
      post share_role_permissions_url, params: { share_role_permission: { permission_description: @share_role_permission.permission_description, share_role_id: @share_role_permission.share_role_id } }, as: :json
    end

    assert_response 201
  end

  test "should show share_role_permission" do
    get share_role_permission_url(@share_role_permission), as: :json
    assert_response :success
  end

  test "should update share_role_permission" do
    patch share_role_permission_url(@share_role_permission), params: { share_role_permission: { permission_description: @share_role_permission.permission_description, share_role_id: @share_role_permission.share_role_id } }, as: :json
    assert_response 200
  end

  test "should destroy share_role_permission" do
    assert_difference('ShareRolePermission.count', -1) do
      delete share_role_permission_url(@share_role_permission), as: :json
    end

    assert_response 204
  end
end
