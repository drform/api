require 'test_helper'

class ShareRolesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @share_role = share_roles(:one)
  end

  test "should get index" do
    get share_roles_url, as: :json
    assert_response :success
  end

  test "should create share_role" do
    assert_difference('ShareRole.count') do
      post share_roles_url, params: { share_role: { role: @share_role.role } }, as: :json
    end

    assert_response 201
  end

  test "should show share_role" do
    get share_role_url(@share_role), as: :json
    assert_response :success
  end

  test "should update share_role" do
    patch share_role_url(@share_role), params: { share_role: { role: @share_role.role } }, as: :json
    assert_response 200
  end

  test "should destroy share_role" do
    assert_difference('ShareRole.count', -1) do
      delete share_role_url(@share_role), as: :json
    end

    assert_response 204
  end
end
