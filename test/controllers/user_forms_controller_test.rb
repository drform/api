require 'test_helper'

class UserFormsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_form = user_forms(:one)
  end

  test "should get index" do
    get user_forms_url, as: :json
    assert_response :success
  end

  test "should create user_form" do
    assert_difference('UserForm.count') do
      post user_forms_url, params: { user_form: { form_id: @user_form.form_id, user_id: @user_form.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show user_form" do
    get user_form_url(@user_form), as: :json
    assert_response :success
  end

  test "should update user_form" do
    patch user_form_url(@user_form), params: { user_form: { form_id: @user_form.form_id, user_id: @user_form.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy user_form" do
    assert_difference('UserForm.count', -1) do
      delete user_form_url(@user_form), as: :json
    end

    assert_response 204
  end
end
