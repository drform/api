Rails.application.routes.draw do
  resources :groups 
  resources :forms do 
    resources :user_forms 
    resources :shares
    post 'export' => 'user_form_items#export'
  end
  resources :user_form_items
  resources :user_forms
  resources :dropdowns
  resources :data_types
  resources :form_items
  resources :sessions
  resources :share_role_permissions
  resources :share_roles
  resources :permissions
  resources :roles
  resources :users
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  put 'users' => 'users#update'
  post 'password/forgot', to: 'passwords#forgot'
  post 'password/reset', to: 'passwords#reset'
  get 'searchusers' => 'users#search'
  delete 'image' => 'user_form_items#destroyImage'
  delete 'form_items' => 'form_items#destroy'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
