class ApplicationMailer < ActionMailer::Base
  default from: "'Mome Forms' <from@example.com>"
  layout 'mailer'
end
