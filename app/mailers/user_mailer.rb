class UserMailer < ApplicationMailer
    def resetpassword_email(user)
        @user = user
        mail(to: @user.email, subject: 'Reset Password')
    end

    def notieditform_email(user,msg, form)
        @form = form
        @msg = msg
        @user = user
        mail(to: @user.email, subject: 'Notifications of Form editing')
    end

    def notieditanswer_email(user, msg, form)
        @form = form
        @msg = msg
        @user = user
        mail(to: @user.email, subject: 'Notifications of Answer editing')
    end

    def notideleteform_email(user,msg, form)
        @form = form
        @msg = msg
        @user = user
        mail(to: @user.email, subject: 'Notifications of Form editing')
    end

    def notideleteanswer_email(user, msg, form)
        @form = form
        @msg = msg
        @user = user
        mail(to: @user.email, subject: 'Notifications of Answer editing')
    end
    
end
