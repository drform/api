class Share < ApplicationRecord
  include Rails.application.routes.url_helpers
    belongs_to :share_role
    belongs_to :user
    belongs_to :form

    def as_json(options = {})     
        super(:except => [:share_role_id, :created_at,:updated_at]).merge!({
            share_role: self.share_role[:role],
            image: !self.user.image.attached? ? nil : rails_blob_path(self.user.image,only_path:true)
        })           
    end
    
    def self.get_share(params)
      if !(params[:form_id].blank?)
        # have form_id
        data = Share.where("form_id = ? and share_role_id = ?", params[:form_id], 2)
        data = data.select %{
            shares.*,  
            users.username
        }  

        data = data.joins "inner join users on users.id = shares.user_id"

        if !(params[:username].blank?)
          data = data.where("users.username ~* ?", params[:username])
        end

        params[:order] ||= 'shares.updated_at desc'
        data = data.order(params[:order])
        data = data.limit(params[:limit]) if params[:limit].present?
        data = data.offset(params[:offset]) if params[:offset].present?

        share = ActiveRecord::Base.connection.execute(data.to_sql)   
        
        shareAr = []
        share.each do |share|
            users = User.find(share["user_id"])
            shareAr << {
                id: share["id"],
                user_id: share["user_id"],
                form_id: share["form_id"],
                share_role_id: share["share_role_id"],
                created_at: share["created_at"],
                name: users["name"],
                email: users["email"],
                image: users.get_image(users.id)
            }
        end
        return shareAr
      else
          return {error: "Don't have form id."}
      end
    end

    def self.new_share(params)
      chkowner = Form.find(params[:form_id])
      chkuser = User.find(params[:user_id])
      if chkowner.user_id == params[:user_id] || chkuser.role_id == 3
        if !(params[:share][:username].blank?)
          # have text
          @user = User.where("username = ?",params[:share][:username].downcase).first
          if !(@user.blank?)
            # have user
            if @user[:role_id] == 2 
              # is user
              chk = Share.where("user_id = ? and form_id = ? ",@user[:id],params[:form_id])
              if chk.blank?
                # Don't have share
                params[:share][:user_id] = @user[:id]
                params[:share][:form_id] = params[:form_id]
                params[:share][:share_role_id] = 2
                @share  = Share.new params.require(:share).permit(:user_id, :form_id, :share_role_id)
                if @share.save
                  return {message: "Share Successfully.",
                          data: @share}
                else
                    return {error: @share.errors }
                end
              else
                return {error: "Shared with this user."}
              end
            else
              return {error: "Can't share with Admin."}
            end
          else
              return {error: "Can't find user."}
          end
        else
            return {error: "Please fill username."}
        end
      else
        return {error: "You are not owner."}
      end
    end

    def self.delete(params)
      chk = Share.where("form_id = ? and share_role_id = 1", params[:form_id]).first
      chkuser = User.find(params[:user_id])
      if chk.user_id == params[:user_id] || chkuser.role_id == 1 || chkuser.role_id == 3
        share = Share.where("user_id = ? and form_id = ?", params[:id], params[:form_id]).first
        share.destroy
        return {message: "Delete Successfully.",
                data: share}
      else
        return {error: "Can not delete this share."}
      end
    end
end
