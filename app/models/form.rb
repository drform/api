class Form < ApplicationRecord
  include Rails.application.routes.url_helpers
  belongs_to :user
  belongs_to :group
  has_many :shares, dependent: :destroy
  has_many :form_items, dependent: :destroy
  has_many :user_forms
  has_one_attached :image
  validates_presence_of :title
  validates_presence_of :description
  validates_uniqueness_of :title, scope: :user_id
  def as_json(options = {})    
    super(:except => [:created_at,:updated_at]).merge!({ 
      # updated_at: Form.get_update(self.id),
      name: self.user.name,
      question: self.form_items.order("order_no ASC"),   
      share: self.shares.as_json(:except => [:created_at,:updated_at]),
      image: !self.image.attached? ? [] : rails_blob_path(self.image,only_path:true)
    })
  end    
   
  def self.get_Datas(form,user_id,params)
    user_answer = UserForm.where("form_id = ? and user_id = ?",form[:id],user_id).limit(params[:limit] ||= 9).order(params[:order] ||= "id ASC").offset(params[:offset] ||= 0)              
    return { form: form,answer: user_answer}
  end 

  def self.form_update(params, user_id)     
    @user = User.find(user_id)
    form = Form.find(params[:id]) 
    if form.user_id == user_id || @user.role_id == 3
      @form = Form.updateform(params) 
      return @form.as_json
    elsif @user.role_id == 1
      if !(params[:message].blank?)
        @form = Form.updateform(params) 
        owner = User.find(form.user_id)
        # SEND EMAIL HERE
        UserMailer.notieditform_email(owner,params[:message],form).deliver_now
        return @form.as_json
      else
        return {error: "Don't have message."}
      end
    else
      return {error: "Can't updated this form."}
    end                   
  end  

  def self.updateform(params)
    @form = Form.find(params[:id])
    if !(params[:form][:image].blank?)
      begin
        puts "*****************************Begin*****************************"    
        params[:form][:image] = User.decode_64(params[:form][:image])
        @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S")
        @form.title = params[:form][:title]
        @form.description = params[:form][:description]      
        if @form.save   
          @form.image.attach(io:File.open(params[:form][:image]), filename: @filename)  
        else
          return {error: @form.errors}    
        end
        puts "*****************************End Begin*****************************" 
      rescue
        puts "*****************************Rescue*****************************"
        @form.title = params[:form][:title]
        @form.description = params[:form][:description]
        @form.image = params[:form][:image]
        if @form.save
        else
          return {error: @form.errors}    
        end
        puts "*****************************End Rescue*****************************"
      end
    else
      @form.title = params[:form][:title]
      @form.description = params[:form][:description]
      if @form.save
      else
        return {error: @form.errors}    
      end    
    end      
    puts "*****************************Start Update Question*****************************"
    if !(params[:form_item].blank?)
      params[:form_item].each do |item|
        if item[:id]
          FormItem.update_form_item(item,@form)
        else
          FormItem.new_form_item(item,@form)
        end 
      end         
    end
    return { message: "Updated Successfully.",
              data: @form.as_json }
  end

  def self.new_form(params,user_id)
    # return params
    if !(params[:form][:user_id].blank?)
      user_id = params[:form][:user_id]
    end
    useradmin = User.where("role_id = 3 and id != ?", user_id)
    if !(params[:form_item].blank?)
      if !(params[:form][:image].blank?)
        begin
          params[:form][:image] = Form.decode_64(params[:form][:image])
          @form = Form.new(            
            user_id: user_id,
            group_id: params[:form][:group_id],
            title: params[:form][:title],
            description: params[:form][:description],
            image: params[:form][:image]
          )        
          if @form.save
            @form.image.attach(io:File.open(@tmp_file), filename: @filename)
            params[:form_item].each do |item|
              FormItem.new_form_item(item,@form)
            end        
            @share = Share.new(user_id: user_id,form_id: @form.id,share_role_id: 1) # 1 = Owner , 2 = Other
            if @share.save 
              cuser = User.find(user_id)
              useradmin.each do |uadmin|
                shr = Share.create(user_id: uadmin[:id],form_id: @form.id,share_role_id: 3)
              end
              return { message: "Save form successfully.",
                      data: @form } 
              
            else
              return {error: @share.errors} 
            end
          else
            return {error: @form.errors} 
          end
        rescue
          puts "image base 64 error"
          @form = Form.new
          @form.user_id = user_id
          @form.group_id = params[:form][:group_id]
          @form.title = params[:form][:title]
          @form.description = params[:form][:description]
          @form.image = params[:form][:image]
          if @form.save       
            params[:form_item].each do |item|
              FormItem.new_form_item(item,@form)
            end 
            @share = Share.new(user_id: user_id,form_id: @form.id,share_role_id: 1) # 1 = Owner , 2 = Other                 
            if @share.save
              cuser = User.find(user_id)
              useradmin.each do |uadmin|
                shr = Share.create(user_id: uadmin[:id],form_id: @form.id,share_role_id: 3)
              end
              return { message: "Save form successfully.",
                        data: @form } 
            else
              return {error: @share.errors} 
            end
          else
            return {error: @form.errors}          
          end
        end 
      else
        @form = Form.new
        @form.user_id = user_id
        @form.group_id = params[:form][:group_id]
        @form.title = params[:form][:title]
        @form.description = params[:form][:description]
        if @form.save       
          @form.image.attach(io: File.open('./form.png'), filename: 'form.png', content_type: 'image/png')
          params[:form_item].each do |item|
            FormItem.new_form_item(item,@form)
          end 
          @share = Share.new(user_id: user_id,form_id: @form.id,share_role_id: 1) # 1 = Owner , 2 = Other                 
          if @share.save
            cuser = User.find(user_id)
            useradmin.each do |uadmin|
              shr = Share.create(user_id: uadmin[:id],form_id: @form.id,share_role_id: 3)
            end
            return { message: "Save form successfully.",
                      data: @form } 
          else
            return {error: @share.errors} 
          end
        else
          return {error: @form.errors}               
        end
      end     
    else
      return {error: "Don't have form_item"}
    end    
  end

  #Get Share and Form (Owner 1, Other 2)
  def self.search_form(params={})
    admin = User.find(params[:user_id])
    limit = (!params[:limit].nil? ? params[:limit] : 10000).to_i
    offset = (!params[:offset].nil? ? params[:offset] : 0).to_i
    if admin.role_id == 1 || (admin.role_id == 3 && (params[:share_role_id].blank?))
      data = Form.all
      data = data.select %{
        forms.*,  
        users.username
      }
      data = data.joins "inner join users on users.id = forms.user_id"
      
    else
      data = self.all
      data = data.select %{
          forms.*,  
          users.username,
          share_roles.role
      }  

      data = data.joins "inner join users on users.id = forms.user_id"

      data = data.joins "inner join shares on shares.form_id = forms.id"

      data = data.joins "inner join share_roles on share_roles.id = shares.share_role_id"

      data = data.where("shares.user_id = ?",  params[:user_id])
    end

    data = data.where("forms.group_id = ?", params[:group_id]) if params[:group_id].present?

    data = data.where("shares.share_role_id = ?", params[:share_role_id]) if params[:share_role_id].present?

    data = data.where("to_char(forms.created_at, 'YYYY-MM-DD') ~* ?", params[:date]) if params[:date].present?

    data = data.where("forms.title ~* ? or forms.description ~* ? or users.username ~* ?", params[:keyword], params[:keyword], params[:keyword])  if params[:keyword].present?

    params[:order] ||= 'forms.updated_at desc'
    data = data.order(params[:order])

    form = ActiveRecord::Base.connection.execute(data.to_sql)    
    
    formAr = []
    form.each do |form|
      forms = Form.find(form["id"]) 
      updated_at =  Form.get_update(forms.id)
      formAr << {
        id: form["id"],
        title: form["title"],
        description: form["description"],
        updated_at: updated_at,
        owner: form["username"],
        share_role: form["role"],
        image: forms.get_image(forms.id)
      }
    end

    results = Form.limit_offset(limit, offset, formAr.sort_by{|u| u[:updated_at]}.reverse)

    return results
  end

  def self.limit_offset(limit, offset, array)
    lstArray = []
    begin 
        if limit > array.count
            limit = (array.count) - offset
        end
        limit.times do |i|
            if !(array[offset+i].blank?)
                lstArray << array[offset+i]
            end
        end
        return lstArray.blank? ? nil : lstArray
    rescue
        return nil
    end
end

  def self.get_update(form_id)
    updated_fit = %{
      select max(updated_at) as updated_at
      from form_items
      where form_id = #{form_id}
    }
    updated_fit = ActiveRecord::Base.connection.execute(updated_fit)
    if !(updated_fit.blank?)
      updated_fit = updated_fit[0]["updated_at"].to_datetime
    else
      updated_fit = 0
    end

    updated_form = %{
      select updated_at
      from forms
      where id = #{form_id}
    }
    updated_form = ActiveRecord::Base.connection.execute(updated_form)
    if !(updated_form.blank?)
      updated_form = updated_form[0]["updated_at"].to_datetime
    else
      updated_form = 0
    end
    
    return [updated_fit, updated_form].max
  end
  
  def self.delete(params)
    if !(params[:user_id].blank?)
      @user = User.find(params[:user_id])
      @form = Form.find(params[:id])
      if @form.user_id == params[:user_id] || @user.role_id == 3
        @form.destroy
        return {message: "Delete Successfully.",
                data: @form}
      elsif  @user.role_id == 1
        if !(params[:message].blank?)
          user = User.find(@form.user_id)
          # SEND EMAIL HERE
          UserMailer.notideleteform_email(user,params[:message],@form).deliver_now
          @form.destroy
          return {message: "Delete form and send email Successfully.",
                  data: @form}
        else
          return {error: "Don't have message."}
        end
      else
        return {error: "Can not delete this form."}
      end
    else
      return {error: "Don't have user_id."}
    end
  end
end

