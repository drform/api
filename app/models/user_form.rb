class UserForm < ApplicationRecord
    Rails.application.routes.url_helpers
    belongs_to :user
    belongs_to :form
    has_many :user_form_items , dependent: :destroy
    def as_json(options = {})
        super.merge!({ 
            name: self.user.name,
            image: !self.user.image.attached? ? nil : rails_blob_path(self.user.image,only_path:true)
        })
    end

    def self.get_filenimage(params)
        limit = (!params[:limit].nil? ? params[:limit] : 10000).to_i
        offset = (!params[:offset].nil? ? params[:offset] : 0).to_i
        if params[:type].downcase == "image"
            image = UserForm.search_filenimage(params[:form_id],10,params[:search],limit,offset,nil)
            return image
        elsif params[:type].downcase == "file"
            filename = params[:filename].nil? ? nil : params[:filename]
            file = UserForm.search_filenimage(params[:form_id],8,params[:search],limit,offset,filename)
            return file
        end
    end

    def self.search_filenimage(form_id,data_type,search,limit,offset,filename)
        array = []
        if !(search.blank?)
            begin
                str = search.split(',')
                if str.count == 2
                    date1 = (str[0].to_date).strftime("%Y-%m-%d")
                    date2 = (str[1].to_date).strftime("%Y-%m-%d")
                    betweendate = %{
                        select date(user_form_items.created_at) as date
                        from user_form_items
                        inner join form_items on form_items.id = user_form_items.form_item_id
                        where form_items.form_id = '#{form_id}' and form_items.data_type_id = '#{data_type}'
                        and date(user_form_items.created_at) BETWEEN '#{date1}' and '#{date2}'
                        group by date(user_form_items.created_at)
                        order by date(user_form_items.created_at) DESC
                    }
                    betweendate = ActiveRecord::Base.connection.execute(betweendate) #select date

                    betweendate.each do |date|
                        ddate = ((date["date"]).to_date).strftime("%Y-%m-%d")
                        array = sort_file(form_id,data_type,ddate.to_s,array,limit,offset,filename)
                    end
                else
                    search = (search.to_date).strftime("%Y-%m-%d")
                    array = sort_file(form_id,data_type,search,array,limit,offset,filename)
                end
                
                if !(array.blank?)
                    return array
                else
                    return nil
                end
            rescue
                return nil
            end
        else
            alldate = %{
                select to_char(user_form_items.updated_at, 'YYYY-MM-DD') as date
                from user_form_items
                inner join form_items on form_items.id = user_form_items.form_item_id
                where form_items.form_id = '#{form_id}' and form_items.data_type_id = '#{data_type}'
                group by to_char(user_form_items.updated_at, 'YYYY-MM-DD')
                order by to_char(user_form_items.updated_at, 'YYYY-MM-DD') DESC
            }
            # alldate = %{
            #     select date(created_at)
            #     from active_storage_attachments
            #     where record_type = 'UserFormItem' and record_id in (
            #         select uitem.id
            #         from user_form_items uitem
            #         inner join user_forms on user_forms.id = uitem.user_form_id
            #         inner join form_items on form_items.id = uitem.form_item_id
            #         where user_forms.form_id = '#{form_id}' and form_items.data_type_id = '#{data_type}')
            #     group by date(created_at)
            #     order by date(created_at) DESC
            # } #--Upload
            alldate = ActiveRecord::Base.connection.execute(alldate) #select date

            alldate.each do |date|
                array = sort_file(form_id,data_type,date["date"],array,limit,offset,filename)
            end
            if !(array.blank?)
                return array
            else
                return nil
            end
        end
    end

    def self.sort_file(form_id,data_type,date,array,limit,offset,filename)
        files = []
        results = []
        file = %{
            select user_form_items.id, user_form_items.updated_at as created_at
            from user_form_items
            inner join form_items on form_items.id = user_form_items.form_item_id
            where form_items.form_id = '#{form_id}' and form_items.data_type_id = '#{data_type}'
            and to_char(user_form_items.updated_at, 'YYYY-MM-DD') = '#{date}'
        }

        if !(filename.blank?)
            file += %{
                and user_form_items.file_value ~* '#{filename}'
            }
        end

        file += %{
            order by user_form_items.user_form_id DESC
        }
        file = ActiveRecord::Base.connection.execute(file) #select user_form

        # at_id = %{
        #     select at.id, at.record_id, at.created_at, at.blob_id
        #     from active_storage_attachments at
        #     inner join user_form_items on user_form_items.id = at.record_id
        #     where true 
        # }
        
        # at_id += %{
        #     and user_form_items.file_value ~* '#{filename}'
        # } if !(filename.blank?)

        # at_id += %{
        #     and at.record_type = 'UserFormItem' and date(created_at) = '#{date}'
        #     and at.record_id in (
        #         select uitem.id
        #         from user_form_items uitem
        #         inner join user_forms on user_forms.id = uitem.user_form_id
        #         inner join form_items on form_items.id = uitem.form_item_id
        #         where user_forms.form_id = '#{form_id}' and form_items.data_type_id = '#{data_type}')
        #     order by at.record_id ASC
        # }
        # at_id = ActiveRecord::Base.connection.execute(at_id) #select record_id --Upload

        file.each do |f|
        # at_id.each do |f| #--Upload
            ufi = UserFormItem.find(f["id"])
            # ufi = UserFormItem.find(f["record_id"])#--Upload
            if data_type == 8
                chkfile = ufi.get_file(ufi["id"])
                if !(chkfile[:id].blank?)
                    files << { 
                        id: chkfile[:id],
                        file: chkfile[:file],
                        filename: chkfile[:filename],
                        created_at: get_created(chkfile[:id])
                    }
                end

                # chkfile = ufi.get_file(ufi["id"])
                # if !(chkfile[:id].blank?)
                #     if chkfile[:id] == f["id"]
                #         files << { 
                #             id: chkfile[:id],
                #             file: chkfile[:file],
                #             filename: chkfile[:filename],
                #             created_at: get_created(chkfile[:id])
                #         }
                #     end
                # end #--Upload
            elsif data_type == 10
                chkimg = ufi.get_images(ufi["id"])
                if !(chkimg.blank?)
                    chkimg.each do |imgAr|
                        files << {
                            id: imgAr[:id],
                            image: imgAr[:image],
                            created_at: get_created(imgAr[:id])
                        }
                    end
                end

                # chkimg = ufi.get_images(ufi["id"])
                # if !(chkimg.blank?)
                #     chkimg.each do |imgAr|
                #         if imgAr[:id] == f["id"]
                #             files << {
                #                 id: imgAr[:id],
                #                 image: imgAr[:image],
                #                 created_at: get_created(imgAr[:id])
                #             }
                #         end
                #     end
                # end #--Upload
            end
        end

        results = UserForm.limit_offset(limit, offset, files)

        if !(results.blank?)
            array << {
                date: date,
                data: results
            }
        end
        return array
    end

    def self.get_created(id)
        sql = %{
            select created_at
            from active_storage_attachments
            where id = '#{id}'
          }
        sql = ActiveRecord::Base.connection.execute(sql)
        date = (sql[0]["created_at"]).to_datetime
    end

    def self.limit_offset(limit, offset, array)
        lstArray = []
        begin 
            if limit > array.count
                limit = (array.count) - offset
            end
            limit.times do |i|
                if !(array[offset+i].blank?)
                    lstArray << array[offset+i]
                end
            end
            return lstArray.blank? ? nil : lstArray
        rescue
            return nil
        end
    end
    
    def self.get_user_form(params)
        if !(params[:type].blank?)
            user_form = UserForm.get_filenimage(params)
            return user_form
        else
            limit = (!params[:limit].nil? ? params[:limit] : 10000).to_i
            offset = (!params[:offset].nil? ? params[:offset] : 0).to_i
            data = UserForm.where("form_id = ?",params[:form_id])
            data = data.select %{
                user_forms.*,
                users.name
            }

            data = data.joins "inner join users on users.id = user_forms.user_id"

            if !(params[:keyword].blank?)
                data = data.where("users.name ~* ?", params[:keyword])
            end

            if !(params[:date].blank?)
                str = params[:date].split(',')
                if str.count == 2
                    date1 = (str[0].to_date).strftime("%Y-%m-%d")
                    date2 = (str[1].to_date).strftime("%Y-%m-%d")
                    data = data.where("date(user_forms.created_at) BETWEEN ? and ? ", date1, date2)
                else
                    date = params[:date].to_date
                    data = data.where("to_char(user_forms.created_at, 'YYYY-MM-DD') ~* ? ", date)
                end
            end

            data = data.order 'user_forms.updated_at desc'

            data = ActiveRecord::Base.connection.execute(data.to_sql)

            amount = data.count
            dataAr = []
            data.each do |uf|
                user = User.find(uf["user_id"])
                dataAr << {
                    id: uf["id"],
                    user_id: uf["user_id"],
                    form_id: uf["form_id"],
                    created_at: uf["created_at"].to_datetime.in_time_zone,
                    updated_at: (UserForm.get_update(uf["id"])).in_time_zone,
                    # updated_at: uf["updated_at"].to_datetime.in_time_zone,
                    name: user["name"],
                    image: user.get_image(user.id)
                }
            end

            results = UserForm.limit_offset(limit, offset, dataAr.sort_by{|u| u[:updated_at]}.reverse)

            form = Form.find(params[:form_id])

            creater_temp = {
                amount: amount,
                user_form: results,
                form: {
                    id: form[:id],
                    title: form[:title],
                    description: form[:description],
                    image: form.get_image(form[:id])
                }
            }
            return creater_temp
        end
    end

    def self.get_update(user_form_id)
        updated_ufit = %{
          select max(updated_at) as updated_at
          from user_form_items
          where user_form_id = #{user_form_id}
        }
        updated_ufit = ActiveRecord::Base.connection.execute(updated_ufit)
        if !(updated_ufit.blank?)
            updated_ufit1 = updated_ufit[0]["updated_at"].to_datetime
        else
            updated_ufit1 = '0000-00-00'
        end
    
        updated_uf = %{
          select updated_at
          from user_forms
          where id = #{user_form_id}
        }
        updated_uf = ActiveRecord::Base.connection.execute(updated_uf)
        if !(updated_uf.blank?)
            updated_uf1 = updated_uf[0]["updated_at"].to_datetime
        else
            updated_uf1 = '0000-00-00'
        end

        return [updated_ufit1, updated_uf1].max
    end

    def self.get_Data(params)
        user_form = UserForm.where("id = ? and form_id = ? ", params[:id], params[:form_id]).first
        if user_form
            data = FormItem.all
            @user = User.find(user_form["user_id"])
            data = data.select %{ 
                form_items.id , form_items.order_no, form_items.data_type_id, data_types.type_value as datatype,
                form_items.title as question, user_form_items.id as user_item_id,
                CASE form_items.data_type_id 
                    WHEN '1'    THEN user_form_items.date_value::varchar
                    WHEN '2'    THEN user_form_items.datetime_value::varchar
                    WHEN '3'    THEN user_form_items.string_value::varchar
                    WHEN '4'    THEN user_form_items.text_value::varchar
                    WHEN '5'    THEN user_form_items.integer_value::varchar
                    WHEN '6'    THEN user_form_items.float_value::varchar
                    WHEN '7'    THEN dropdowns.dropdown_value::varchar
                    WHEN '8'    THEN user_form_items.file_value::varchar
                    WHEN '9'    THEN dropdowns.dropdown_value::varchar
                    WHEN '10'    THEN user_form_items.image_value::varchar
                END AS answer,
                CASE user_form_items.boolean_value
                    WHEN 'true'    THEN user_form_items.boolean_value::varchar
                    WHEN 'false'    THEN user_form_items.boolean_value::varchar
                    ELSE ''
                END AS boolean
            }  

            data = data.joins "inner join user_forms on user_forms.form_id = form_items.form_id"

            data = data.joins "left join user_form_items on user_form_items.user_form_id = user_forms.id and user_form_items.form_item_id = form_items.id"

            data = data.joins "inner join users on users.id = user_forms.user_id"

            data = data.joins "inner join data_types on data_types.id = form_items.data_type_id"

            data = data.joins "left join dropdowns on dropdowns.id = user_form_items.dropdown_value_id 
            OR dropdowns.id = user_form_items.checkbox_value::integer"

            data = data.where("user_forms.id = ?", user_form.id)

            if !(params[:keyword].blank?)
                data = data.where("form_items.title ~* ? or form_items.order_no = ?", params[:keyword], params[:keyword].to_i)
            end

            data = data.group "user_form_items.user_form_id, user_forms.id,users.id, form_items.id, user_form_items.id, dropdowns.id, data_types.id"

            params[:order] ||= 'form_items.order_no asc'
            data = data.order(params[:order])
            data = data.limit(params[:limit]) if params[:limit].present?
            data = data.offset(params[:offset]) if params[:offset].present?

            data = ActiveRecord::Base.connection.execute(data.to_sql)

            dataa = []
            numchk = 0
            data.each do |dt|
                if !(dt["id"] == numchk)
                    if !(dt["user_item_id"].blank?) || !(dt["answer"].blank?)
                        if dt["data_type_id"] == 7
                            sql = %{
                                select user_form_items.id as user_item_id, dropdowns.id as Choice_id , dropdowns.dropdown_value as value
                                from user_form_items
                                inner join dropdowns on dropdowns.id = user_form_items.checkbox_value::integer
                                where user_form_items.form_item_id = '#{dt["id"]}' and user_form_items.user_form_id = '#{user_form.id}'
                                and user_form_items.boolean_value = true
                                order by dropdowns.id ASC
                            }
                            sql = ActiveRecord::Base.connection.execute(sql)
                            numchk = dt["id"]
                        elsif dt["data_type_id"] == 8
                            uitem = UserFormItem.find(dt["user_item_id"])
                            file = uitem.get_file(uitem.id)
                            sql = [{
                                user_item_id: dt["user_item_id"],
                                value: file[:file],
                                filename: file[:filename]
                            }]
                        elsif dt["data_type_id"] == 10
                            uitem = UserFormItem.find(dt["user_item_id"])
                            sql = [{
                                user_item_id: dt["user_item_id"],
                                values: uitem.get_images(uitem.id)
                            }]
                        else
                                sql = [{
                                    user_item_id: dt["user_item_id"],
                                    value: dt["answer"]
                                }]
                        end
                    else
                        sql = nil
                    end

                    dataa << {
                        id: dt["id"],
                        order_no: dt["order_no"],
                        data_type_id: dt["data_type_id"],
                        question: dt["question"],
                        answer: sql
                    }
                end
            end

            sql = %{
                select * 
                from user_forms
                where id = #{params["id"]}
            }
            sql = ActiveRecord::Base.connection.execute(sql)

            creater_temp = {
                data: dataa,
                creater: {
                    user_id: @user["id"],
                    username: @user["name"],
                    created_at: (UserForm.get_update(params[:id])).to_datetime.in_time_zone,
                    # sql[0]["updated_at"].to_datetime.in_time_zone,
                    image: @user.get_image(@user["id"])
                }
            }
            return creater_temp
        else
            return {error: "Can not find this User_form."}
        end
    end
    
    def self.new_user_form(params,user_id)
        if !(params[:user_form_item].blank?)
            puts "1. Save UserForm"
            user_form = UserForm.new
            user_form.user_id = user_id
            user_form.form_id = params[:form_id]
            puts "End Set Form"
            if user_form.save
                # 2. Save User_Form_Items
                params[:user_form_item].each do |item|
                    form_item = FormItem.where("form_id = ? and order_no = ?",user_form[:form_id],item[:order_no].to_i).first
                    @answer = UserFormItem.new_answer(form_item.data_type.ref_index,user_form[:id], form_item[:id], item[:value], item[:boolean], params )              
                end # end Loop
                return {message: "Save successfully.",
                        data: user_form}
                # end
            else
                return {error: "Can't save user_form"}
            end
            # end
        else
            return {error: "Don't have user_form_item"}
        end
    end  

    def self.user_answer_update(params,user_id)
        if !(params[:user_form_item].blank?)
            # 1. Save UserForm    
            chkuser = User.find(user_id)    
            user_form = UserForm.find(params[:id]) 
            if user_form.user_id == user_id || chkuser.role_id == 3
                params[:user_form_item].each do |item|                                   
                    form_item = FormItem.where("form_id = ? and order_no = ?",user_form[:form_id],item[:order_no].to_i).first                
                    # return form_item,item
                    @answer = UserFormItem.update_answer(params, form_item.id, form_item.data_type.ref_index, item[:value], item[:boolean])
                end # end Loop
                return { message: "Update Successfully.",
                        data: user_form}
                # end   
            elsif chkuser.role_id == 1
                if !(params[:message].blank?)
                    params[:user_form_item].each do |item|                                   
                        form_item = FormItem.where("form_id = ? and order_no = ?",user_form[:form_id],item[:order_no].to_i).first                
                        # return form_item,item
                        @answer = UserFormItem.update_answer(params, form_item.id, form_item.data_type.ref_index, item[:value], item[:boolean])
                    end # end Loop
                    @form = Form.find(user_form.form_id)
                    owner = User.find(user_form.user_id)
                    # SEND EMAIL HERE
                    UserMailer.notieditanswer_email(owner,params[:message],@form).deliver_now
                    return { message: "Update Successfully.",
                            data: user_form}
                else
                    return {error: "Don't have message."}
                end  
            else
                return {error: "Can't update this answer."}
            end
        else
            return { message: "No Updates."}
        end
    end

    def self.delete(params)
        if !(params[:user_id].blank?)
            @user = User.find(params[:user_id])
            @form = Form.find(params[:form_id])
            @user_form = UserForm.find(params[:id])
            if @user_form.user_id == params[:user_id] || @user.role_id == 3
                @user_form.destroy
                return {message: "Delete Successfully.",
                        data: @user_form}
            elsif  @user.role_id == 1
                if !(params[:message].blank?)
                    user = User.find(@user_form.user_id)
                    # SEND EMAIL HERE
                    UserMailer.notideleteanswer_email(user,params[:message],@form).deliver_now
                    @user_form.destroy
                    return {message: "Delete user_form and send email Successfully.",
                            data: @user_form}
                else
                    return {error: "Don't have message."}
                end
            else
                return {error: "Can not delete this user_form."}
            end
        else
            return {error: "Don't have user_id."}
        end
    end
end

