class UserFormItem < ApplicationRecord
    belongs_to :form_item
    belongs_to :user_form
    has_many_attached :image
    has_one_attached :file
    validates_presence_of :user_form_id
    validates_presence_of :form_item_id
    def as_json(options={})           
        super(:only => [
            :id,
            :user_form_id,
            :form_item_id
        ]).merge!({
            value: UserFormItem.get_Attribute(self.id)            
        })          
    end  
    def self.search(params) 
      dataAr = []
      if !(params[:user_form_item].blank?)
        puts params.inspect
        pointer = 1        
        sql = %{
            select user_forms.*, users.name
            from user_form_items uitem
        }    
        params[:user_form_item].each do |data|
          data[:value] = data[:value].to_s
            case data[:type].to_i
            when 1 #Can find Between Date
              str = data[:value].split('t')
              if str.count == 2
                sql += %{
                  right join (
                    select user_form_id
                    from user_form_items
                    where date_value BETWEEN '#{str[0].to_date}' and '#{str[1].to_date}'
                    and form_item_id = '#{data[:form_item_id].to_i}'
                  ) d#{pointer} on d#{pointer}.user_form_id = uitem.user_form_id
                }
              else
                sql += %{
                  right join (
                    select user_form_id
                    from user_form_items
                    where date_value = '#{data[:value].to_date}' and form_item_id = '#{data[:form_item_id].to_i}'
                  ) d#{pointer} on d#{pointer}.user_form_id = uitem.user_form_id
                }
              end
            when 2 #Can find Between Date
              str = data[:value].split('t')
              if str.count == 2
                sql += %{
                  right join (
                    select user_form_id
                    from user_form_items
                    where datetime_value BETWEEN '#{str[0].to_datetime}' and '#{str[1].to_datetime}'
                    and form_item_id = '#{data[:form_item_id].to_i}'
                  ) d#{pointer} on d#{pointer}.user_form_id = uitem.user_form_id
                }
              else
                sql += %{
                  right join (
                    select user_form_id
                    from user_form_items
                    where datetime_value = '#{data[:value].to_datetime}' and form_item_id = '#{data[:form_item_id].to_i}'
                  ) d#{pointer} on d#{pointer}.user_form_id = uitem.user_form_id
                }
              end                
            when 3
              sql += %{
                right join (
                  select user_form_id
                  from user_form_items
                  where string_value ~* '#{data[:value]}' and form_item_id = '#{data[:form_item_id].to_i}'
                ) d#{pointer} on d#{pointer}.user_form_id = uitem.user_form_id
              }
            when 4 
              sql += %{
                right join (
                  select user_form_id
                  from user_form_items
                  where text_value ~* '#{data[:value]}' and form_item_id = '#{data[:form_item_id].to_i}'
                ) d#{pointer} on d#{pointer}.user_form_id = uitem.user_form_id
              }
            when 5 #Can find Between Value
              str = data[:value].split('t')
              if str.count == 2
                sql += %{
                  right join (
                    select user_form_id
                    from user_form_items
                    where integer_value BETWEEN '#{str[0].to_i}' and '#{str[1].to_i}'
                    and form_item_id = '#{data[:form_item_id].to_i}'
                  ) d#{pointer} on d#{pointer}.user_form_id = uitem.user_form_id
                }
              else
                sql += %{
                  right join (
                    select user_form_id
                    from user_form_items
                    where integer_value = #{data[:value].to_i} and form_item_id = '#{data[:form_item_id].to_i}'
                  ) d#{pointer} on d#{pointer}.user_form_id = uitem.user_form_id
                }
              end         
            when 6 #Can find Between Value
              str = data[:value].split('t')
              if str.count == 2
                sql += %{
                  right join (
                    select user_form_id
                    from user_form_items
                    where float_value BETWEEN '#{str[0].to_f}' and '#{str[1].to_f}'
                    and form_item_id = '#{data[:form_item_id].to_i}'
                  ) d#{pointer} on d#{pointer}.user_form_id = uitem.user_form_id
                }
              else
                sql += %{
                  right join (
                    select user_form_id
                    from user_form_items
                    where float_value = '#{data[:value].to_f}' and form_item_id = '#{data[:form_item_id].to_i}'
                  ) d#{pointer} on d#{pointer}.user_form_id = uitem.user_form_id
                }
              end              
            when 7
              str = data[:value].split(',')
              num = []
              str.each do |str|
                num << str.to_i
              end
              number = (num.to_s.delete "[").delete "]"
                sql += %{
                  right join (
                    select user_form_id
                    from user_form_items
                    where checkbox_value in (#{number}) and boolean_value = true and form_item_id = '#{data[:form_item_id].to_i}'
                  ) d#{pointer} on d#{pointer}.user_form_id = uitem.user_form_id
                }
            when 9
              str = data[:value].split(',')
              num = []
              str.each do |str|
                num << str.to_i
              end
              number = (num.to_s.delete "[").delete "]"
                sql += %{
                  right join (
                    select user_form_id
                    from user_form_items
                    where dropdown_value_id in (#{number}) and form_item_id = '#{data[:form_item_id].to_i}'
                  ) d#{pointer} on d#{pointer}.user_form_id = uitem.user_form_id
                }
            else
                return { message: "Not found data." }
            end            
            pointer += 1
        end

        sql += %{
          inner join user_forms on user_forms.id = uitem.user_form_id
          left join users on users.id = user_forms.user_id
          where true
        }

        if !(params[:search].blank?)
          sql += %{
            and users.name ~* '#{params[:search]}'
          }
        end

        if !(params[:date].blank?)
          str = params[:date].split(',')
                if str.count == 2
                    date1 = (str[0].to_date).strftime("%Y-%m-%d")
                    date2 = (str[1].to_date).strftime("%Y-%m-%d")
                    sql += %{
                     and date(user_forms.created_at) BETWEEN '#{date1}' and '#{date2}'
                    }
                else
                    date = params[:date].to_date
                    sql += %{
                     and to_char(user_forms.created_at, 'YYYY-MM-DD') ~* '#{date}'
                    }
                end
        end

        sql += %{
          group by uitem.user_form_id, user_forms.id, users.id
        }

        amounts = sql
        sql += %{
          order by #{params[:order]}
        } if params[:order].present?

        sql += %{
          limit #{params[:limit]}
        } if params[:limit].present?

        sql += %{
          offset #{params[:offset]}
        } if params[:offset].present?
      
        sql = ActiveRecord::Base.connection.execute(sql)
        amounts = ActiveRecord::Base.connection.execute(amounts)
        amount = amounts.count

        sql.each do |uf|
            user = User.find(uf["user_id"])
            dataAr << {
                id: uf["id"],
                user_id: uf["user_id"],
                form_id: uf["form_id"],
                created_at: uf["created_at"].to_datetime,
                updated_at: (UserForm.get_update(uf["id"])).to_datetime.in_time_zone,
                name: uf["name"],
                image: user.get_image(user.id)
            }
        end
        if dataAr.blank?
          dataAr = nil
          return dataAr
        end
        return { amount:  amount,
          user_form: dataAr.sort_by{|u| u[:updated_at]}.reverse}
      else
        return nil
      end
    end
  
    def self.get_Attribute(id)
        @user_form_items = UserFormItem.find(id)
        if !@user_form_items[:date_value].nil?
          return @user_form_items[:date_value]
        elsif !@user_form_items[:datetime_value].nil?
          return @user_form_items[:datetime_value]
        elsif !@user_form_items[:string_value].nil?
          return @user_form_items[:string_value]
        elsif !@user_form_items[:text_value].nil?
          return @user_form_items[:text_value]
        elsif !@user_form_items[:integer_value].nil?
          return @user_form_items[:integer_value]
        elsif !@user_form_items[:float_value].nil?
          return @user_form_items[:float_value]
        elsif !@user_form_items[:checkbox_value].nil?
          return @user_form_items[:checkbox_value]
        elsif !@user_form_items[:file_value].nil?
          return @user_form_items[:file_value]
        elsif !@user_form_items[:dropdown_value_id].nil? 
          data = Dropdown.find(@user_form_items[:dropdown_value_id])
          return data[:dropdown_value]
        elsif !@user_form_items[:image_value].nil?
          return @user_form_items[:image_value]
        end      
    end

    def self.new_answer(form_data_ref, user_form_id, form_item_id, item_value, item_boolean, params )
      # require 'mini_magick'
      @answer = UserFormItem.new
      @answer.user_form_id = user_form_id
      @answer.form_item_id = form_item_id
      if form_data_ref == 1                                       
          @answer.date_value =item_value.to_date   
      elsif form_data_ref == 2
          @answer.datetime_value =item_value.to_datetime.in_time_zone
      elsif form_data_ref == 3
          @answer.string_value =item_value
      elsif form_data_ref == 4
          @answer.text_value =item_value
      elsif form_data_ref == 5
          @answer.integer_value =item_value
      elsif form_data_ref == 6
          @answer.float_value =item_value
      elsif form_data_ref == 7
        @answer.checkbox_value =item_value
        @answer.boolean_value = item_boolean
      elsif form_data_ref == 8
          params[:file] =item_value
          @answer.file.attach(params[:file])
          @answer.file_value = @answer.get_filename(@answer.id)
      elsif form_data_ref == 9
          @answer.dropdown_value_id = item_value   
      elsif form_data_ref == 10
        begin
          temp_image = []
          item_value.each do |itvalue|
            @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S") + ".jpg"
            params[:image] = UserFormItem.decode_64(itvalue)
            temp_image << {path: params[:image], filename: @filename}
          end
          temp_image.each do |item|
            @answer.image.attach(io:File.open(item[:path]), filename: item[:filename])	
          end
        rescue
            params[:image] = item_value
            @answer.image.attach(params[:image])
        end   
        @answer.image_value = (item_value.count).to_s + " image"                   
      end #end if nil?

      if @answer.save
          puts "save"
      else
          return @answer.errors
      end
    end

    def self.update_answer(params, form_item_id, form_data_ref, item_value, item_boolean)
      if !(form_data_ref == 7)
        @answer = UserFormItem.where("user_form_id = ? and form_item_id = ?",params[:id],form_item_id).first 
      elsif form_data_ref == 7
        @answer = UserFormItem.where("user_form_id = ? and form_item_id = ? and checkbox_value = ? ",params[:id],form_item_id, item_value).first 
      end

      if !(@answer.blank?)
        if form_data_ref == 1                                       
          @answer.date_value = item_value     
        elsif form_data_ref == 2
            @answer.datetime_value = item_value
        elsif form_data_ref == 3
            @answer.string_value = item_value
        elsif form_data_ref == 4
            @answer.text_value = item_value
        elsif form_data_ref == 5
            @answer.integer_value = item_value
        elsif form_data_ref == 6
            @answer.float_value = item_value
        elsif form_data_ref == 7
          @answer.checkbox_value = item_value
          @answer.boolean_value = item_boolean
        elsif form_data_ref == 8
          if !(item_value.blank?)
            params[:file] = item_value
            @answer.file.attach(params[:file])
            @answer.file_value = @answer.get_filename(@answer.id)
          end
        elsif form_data_ref == 9
            @answer.dropdown_value_id = item_value     
        elsif form_data_ref == 10   
          if !(item_value.blank?)
            begin
              temp_image = []
              item_value.each do |itvalue|
                @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S") + ".jpg"
                params[:image] = UserFormItem.decode_64(itvalue)
                temp_image << {path: params[:image], filename: @filename}
              end
              temp_image.each do |item|
                @answer.image.attach(io:File.open(item[:path]), filename: item[:filename])	
              end
            rescue
              params[:image] =item_value
              @answer.image.attach(params[:image])
            end
            @answer.image_value = ((((@answer.image_value).delete " image").to_i) + (item_value.count)).to_s + " image"  
          end                    
        end #end if nil?
        if @answer.save
            puts "save"
        else
            return @answer.errors
        end
      else
        @answer = UserFormItem.new_answer(form_data_ref, params[:id], form_item_id, item_value, item_boolean, params )
        return @answer
      end
    end
    
    def self.export_file(params,user_id)
      require 'csv'
      if !(params[:user_form_item].blank?)
        user_form_item = UserFormItem.search(params)
      else
        user_form_item = UserForm.get_user_form(params)
      end

      @form_item = FormItem.where("form_id = ?", params[:form_id]).order("order_no ASC")

      file = "#{Rails.root}/user_form_items.csv"

      if !(params[:column].blank?)
        number = params[:column].split(',')
      end
      
      column_headers = ["Answer Owner"]
      @form_item.each do |fitem|
        if !(fitem.data_type_id == 8 || fitem.data_type_id == 10)
          if !(params[:column].blank?)
            number.each do |num|
              if fitem[:order_no].to_i == num.to_i
                column_headers << fitem[:title]
              end
            end
          else
            column_headers << fitem[:title]
          end
        end
      end

      if !(column_headers[1].blank?)
        CSV.open(file, 'w', write_headers: true, headers: column_headers) do |writer|
          user_form_item[:user_form].each_with_index do |ufid,index|
            ar = []
            ar << ufid[:name]
            params[:id] = ufid[:id]
            params[:form_id] = user_form_item[:user_form][0][:form_id]
            @user_form = UserForm.get_Data(params)
            @user_form[:data].each do |data|
              if !(data[:data_type_id] == 8 || data[:data_type_id] == 10)
                if !(params[:column].blank?)
                  number.each do |num|
                    if data[:order_no].to_i == num.to_i
                      UserFormItem.chk_answer(data,ar,data[:data_type_id])
                    end
                  end
                else
                  UserFormItem.chk_answer(data,ar,data[:data_type_id])
                end
              end
            end
            writer << ar
          end
        end

        exportfile = User.save_export(user_id)
        return {url: exportfile}
      else
        return {error: "Can't export answer."}
      end
    end

    def self.chk_answer(data,ar,datatype)
      if !(data[:answer].blank?)
        if data[:answer].count > 1
          answer = []
          data[:answer].each do |ans|
            answer << ans["value"]
          end
          ar << (((answer.to_s.delete "[").delete "]").delete '"')
        elsif datatype == 1
          begin
            date = (data[:answer][0][:value]).to_date.strftime("%d-%m-%Y")
          rescue
            date = data[:answer][0][:value]
          end
          ar << date
        elsif datatype == 2
          begin
            date = (data[:answer][0][:value]).to_datetime.strftime("%d-%m-%Y %I:%M:%S")
          rescue
            date = data[:answer][0][:value]
          end
          ar << date
        else
          ar << (!data[:answer][0][:value].nil? ? data[:answer][0][:value] : data[:answer][0][:values])
        end
      else
        ar << data[:answer]
      end
    end

    def self.delete_image(params)
      if !(params[:user_id].blank?)
        params[:images].each do |image|
          @user = User.find(params[:user_id])
          @user_form_item = UserFormItem.find(image[:user_item_id])
          @user_form = UserForm.find(@user_form_item.user_form_id)
          @form = Form.find(@user_form.form_id)
          if @user_form.user_id == params[:user_id] || @user.role_id == 3
            @user_form_item.image.find_by_id(image[:image_id]).purge
            @user_form_item.image_value = ((((@user_form_item.image_value).delete " image").to_i) - 1).to_s + " image"  
            @user_form_item.save
          elsif  @user.role_id == 1
            if !(params[:message].blank?)
                user = User.find(@user_form.user_id)
                # SEND EMAIL HERE
                UserMailer.notieditanswer_email(user,params[:message],@form).deliver_now
                @user_form_item.image.find_by_id(image[:image_id]).purge
                @user_form_item.image_value = ((((@user_form_item.image_value).delete " image").to_i) - 1).to_s + " image"  
                @user_form_item.save
            else
                return {error: "Don't have message."}
            end
          end
        end
        return {message: "Delete image Successfully."}
      else
        return {error: "Don't have user_id."}
      end
    end
  end
