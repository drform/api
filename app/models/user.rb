class User < ApplicationRecord
    include Rails.application.routes.url_helpers
    belongs_to :role
    has_many :shares, dependent: :destroy
    has_many :forms
    has_many :user_forms
    has_many :sessions, dependent: :destroy
    has_one_attached :image
    has_one_attached :exportfile
    validates :username, length: { minimum: 5 }, presence: true, uniqueness: true
    validates :password, length: { minimum: 5 }
    validates :name, length: { minimum: 2 }
    validates :email, format: { with: URI::MailTo::EMAIL_REGEXP } , uniqueness: true
    validates :tel, length: { maximum: 10 ,minimum: 10}, uniqueness: true
    validates :position, length: { minimum: 2 }

    def as_json(options = {})     
        super(:except => [:password, :reset_password_token, :reset_password_sent_at]).merge!({
            role: self.role[:role],
            permission: self.role.permission[:permission_description],
            image: !self.image.attached? ? nil : rails_blob_path(self.image,only_path:true)
        })           
    end    
    def self.new_register(params)        
        params[:username] = params[:username].downcase
        params[:password] = Digest::SHA2.new(512).hexdigest(params[:password])

        if !(params[:image].blank?)
            begin
                params[:image] = User.decode_64(params[:image])
                @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S")
                @user = User.new(params)   
                if @user.save
                    @user.image.attach(io:File.open(params[:image]), filename: @filename)	
                    if @user.role_id == 3
                        shr = User.new_share_useradmin(@user[:id])
                    end
                    return { message: "Sign up Complete.",
                        user_info: @user}        
                else
                    return { error: @user.errors }
                end
            rescue
                @user = User.new(params)
                if @user.save
                    if @user.role_id == 3
                        shr = User.new_share_useradmin(@user[:id])
                    end
                    return { message: "Sign up Complete.",
                            user_info: @user}
                else
                    return { error: @user.errors }
                end
            end
        else
            @user = User.new(params)
            if @user.save
                @user.image.attach(io: File.open('./user.png'), filename: 'user.png', content_type: 'image/png')
                if @user.role_id == 3
                    shr = User.new_share_useradmin(@user[:id])
                end
                return { message: "Sign up Complete.",
                    user_info: @user}
            else
                return { error: @user.errors }
            end
        end
    end


    def self.update(params,user_id)
        if !(params[:user][:user_id].blank?)
            #for admin
            chk = User.where("id = ? ", user_id).first
            if chk[:role_id] == 1 || chk[:role_id] == 3
                @user = User.where("id = ? ", params[:user][:user_id]).first
                oldrole = @user.role_id
                if !(params[:user][:image].blank?)
                    params[:user][:image] = User.decode_64(params[:user][:image]) 
                    params[:user][:id] = params[:user][:user_id]
                    @user.image.attach(io:File.open(params[:user][:image]), filename: @filename)
                    @user.update params.require(:user).permit(:id, :name, :email, :tel, :position, :role_id)
                    newrole = params[:user][:role_id]
                    shr = User.update_share_useradmin(@user[:id], oldrole, newrole.to_i)
                    return { message: "Update successfully.",
                        user_info: @user } 
                elsif @user.update params.require(:user).permit(:id, :name, :email, :tel, :position, :role_id)
                    newrole = params[:user][:role_id]
                    shr = User.update_share_useradmin(@user[:id], oldrole, newrole.to_i)
                    return { message: "Update successfully.",
                        user_info: @user }
                else
                    return {error: @user.errors }
                end
            else
                return { error: "Can't edit this user."}
            end
        else
            #for User
            @user = User.where("id = ? ", user_id).first
            params[:user][:id] = user_id
            #Change password
            if !(params[:user][:password_new].blank?)
                oldPass = @user[:password]
                chkPass = Digest::SHA2.new(512).hexdigest(params[:user][:password])
                if oldPass == chkPass
                    newPass = Digest::SHA2.new(512).hexdigest(params[:user][:password_new])
                    params[:user][:password] = newPass
                    if @user.update params.require(:user).permit(:id, :password)
                        return { message: "Update password successfully.",
                            user_info: @user }
                    else
                        return {error: @user.errors }
                    end
                else
                    return {error: "Incorrect password."}
                end
            else
            #Update Profile
                begin
                    params[:user][:image] = User.decode_64(params[:user][:image])   
                    @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S")       
                    if @user.update params.require(:user).permit(:id, :name, :email, :tel, :position)
                        @user.image.attach(io:File.open(params[:user][:image]), filename: @filename)
                        return { message: "Update successfully.",
                            user_info: @user }
                    else
                        return {error: @user.errors }
                    end
                rescue
                    if @user.update params.require(:user).permit(:id, :name, :email, :tel, :position, :image)
                        return { message: "Update successfully.",
                            user_info: @user }
                    else
                        return {error: @user.errors }
                    end
                end
            end
        end
    end

    def self.new_share_useradmin(user_id)
        form_ids = %{
            select DISTINCT form_id 
            from shares
            where share_role_id = 3
            order by form_id
        }
        form_ids = ActiveRecord::Base.connection.execute(form_ids)

        form_ids.each do |form_id|
            shr = Share.create(user_id: user_id,form_id: form_id,share_role_id: 3)
        end
    end

    def self.update_share_useradmin(user_id, oldrole, newrole)
        if (oldrole == 2 && newrole == 3)
            form_ids = %{
                select DISTINCT form_id 
                from shares
                where share_role_id = 3
                and form_id not in (
                    select form_id
                    from shares
                    where share_role_id = 1
                    and user_id = '#{user_id}'
                )
                and user_id != '#{user_id}'
                order by form_id
            }
            form_ids = ActiveRecord::Base.connection.execute(form_ids)

            form_ids.each do |form_id|
                shr = Share.create(user_id: user_id,form_id: form_id["form_id"],share_role_id: 3)
            end

        elsif (oldrole == 3 && newrole == 2)
            shrs = Share.where("user_id = ? and share_role_id = ?", user_id, 3)
            # return shrs
            shrs.each do |shr|
                shr.destroy
            end
            puts "delete share"
        end
    end
    
    def generate_password_token!
        self.reset_password_token = generate_token
        self.reset_password_sent_at = Time.now.utc
        save!
    end
       
    def password_token_valid?
        (self.reset_password_sent_at + 15.minutes) > Time.now.utc
    end
       
    def reset_password!(password)
        self.reset_password_token = nil
        self.password = Digest::SHA2.new(512).hexdigest(password)
        save!
    end
       
    private
    
    def generate_token
        SecureRandom.hex(3)
    end

    #forgot password and send email
    def self.forgot(params)
        if params[:user][:email].blank? # check if email is present
          return {error: 'Email not present.'}
        end
    
        user = User.find_by(email: params[:user][:email]) # if present find user by email
    
        if user.present?
          user.generate_password_token! #generate pass token
          # SEND EMAIL HERE
          UserMailer.resetpassword_email(user).deliver_now
          return { message: 'Email reset password sent.' }
        else
          return {error: ['Email address not found. Please check and try again.']}
        end
    end
    
    #reset password
    def self.reset(params)
        if params[:user][:password].blank?
            token = params[:user][:token].to_s

            user = User.find_by(reset_password_token: token)

            if user.present? && user.password_token_valid?
                return {message: 'Correct token.'}
            else
                return {error:  ['Token not valid or expired. Try generating a new token.']}
            end
        else
            token = params[:user][:token].to_s

            user = User.find_by(reset_password_token: token)

            if user.present? && user.password_token_valid?
                if user.reset_password!(params[:user][:password])
                    return {message: 'Password reset successfully.'}
                else
                    return {error: user.errors.full_messages}
                end
            else
                return {error:  ['Token not valid or expired. Try generating a new token.']}
            end
        end
    end

    def self.search_user(params, user_id_current)
        if !(params[:username].blank?)
            #search for user
            user = User.where("username ~* ? and role_id = ?", params[:username], 2).limit(params[:limit] ||= 5).offset(params[:offset] ||= 0).order(params[:order] ||= 'users.updated_at desc')
            userAr = []
            user.each do |user|
                users = User.find(user["id"])
                userAr << {
                    id: user["id"],
                    username: user["username"],
                    name: user["name"],
                    email: user["email"],
                    tel: user["tel"],
                    image: users.get_image(users.id)
                }
            end
            return {users: userAr} 
        elsif !(params[:keyword].blank?)
            #search by keyword for admin
            users = User.where("role_id = 2 or role_id = 3")
            user = users.where("name ~* ? or position ~* ? or tel ~* ? ", params[:keyword], params[:keyword], params[:keyword]).limit(params[:limit] ||= 5).offset(params[:offset] ||= 0).order(params[:order] ||= 'users.updated_at desc')
            return user.as_json
        elsif !(params[:role_id].blank?)
            #search by role_id (2, 3) for admin
            users = User.where("role_id = 2 or role_id = 3")
            user = users.where("role_id = ?", params[:role_id]).limit(params[:limit] ||= 5).offset(params[:offset] ||= 0).order(params[:order] ||= 'users.updated_at desc')
            return user.as_json
        else
            chkuser = User.find(user_id_current)
            if chkuser.role_id == 2 
                return chkuser.as_json
            elsif chkuser.role_id == 3
                if !(params[:type].blank?) #All user for useradmin
                    if !(params[:search].blank?)
                        user = User.where(" (role_id != 1 and id != ? and username ~* ? ) or ( role_id != 1 and id != ? and name ~* ?)", chkuser.id, params[:search], chkuser.id, params[:search]).limit(params[:limit] ||= 5).offset(params[:offset] ||= 0).order(params[:order] ||= 'users.updated_at desc')
                    else
                        user = User.where("role_id != 1 and id != ?", chkuser.id).order(params[:order] ||= "id ASC").offset(params[:offset] ||= 0).limit(params[:limit] ||= 10)
                    end
                    return user.as_json
                else
                    return chkuser.as_json
                end      
            else
                user = User.all.order(params[:order] ||= "id ASC").offset(params[:offset] ||= 0).limit(params[:limit] ||= 10)
                return user.as_json
            end
        end
    end

    def self.delete(params)
        if !(params[:user_id].blank?)
            chk = User.find(params[:user_id])
            @user = User.find(params[:id])
            if chk.role_id == 1 || chk.role_id == 3
                @user.destroy
                return {message: "Delete Successfully",
                        data: @user}
            else
                return {error: "Can not delete this user."}
            end
        else
            return {error: "Don't have user_id."}
        end
    end

    def self.save_export(user_id)
        user = User.find(user_id)
        @filename = "answer_" + Time.now.strftime("%Y%m%d%I%M%S") + ".csv"
        user.exportfile.attach(io: File.open('./user_form_items.csv'), filename: @filename , content_type: 'text/csv')
        url = user.get_export_file(user.id)
        return url
    end
end
