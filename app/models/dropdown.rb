class Dropdown < ApplicationRecord
  belongs_to :form_item
  def self.new_dropdown(item,form_item)
    dropdown = Dropdown.new
    dropdown.form_item_id = form_item.id
    dropdown.dropdown_value = item[:data]
    if dropdown.save
        puts "save."
    else
        return {error: dropdown.errors}
    end      
  end
  def self.update_dropdown(item,form_item)   
    dropdown = Dropdown.where("id = ?",item[:id]).first
    dropdown.dropdown_value = item[:data]
    if dropdown.save
      puts "save."
    else
      return {error: dropdown.errors}
    end
  end
end
