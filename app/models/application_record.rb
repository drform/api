class ApplicationRecord < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  self.abstract_class = true

  def self.decode_64(image)
    @decoded_file = Base64.decode64(image)        
    @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S") + ".jpg"
    @tmp_file = Tempfile.new(@filename)        # This creates an in-memory file 
    @tmp_file.binmode                          # This helps writing the file in binary mode.
    @tmp_file.write @decoded_file					
    @tmp_file.rewind()
    image = @tmp_file
    return image
  end

  #Get Image
  def get_image(id)    
    img = !self.image.attached? ? nil : rails_blob_path(self.image,only_path:true)
  end  

  #Get Images
  def get_images(id)
    imgs = !self.image.attached? ? nil : self.image.map{ |img| rails_blob_path(img,only_path:true) }
    ids = !self.image.attached? ? nil : self.image.map{ |img| img.id}
    results = []
    if !(imgs.blank?)
      imgs.each_with_index do |img, i|
        results << {
          user_item_id: id,
          id: ids[i],
          image: img
        }
      end
    else 
      results = nil
    end
    results
  end 

  #Get filename
  def get_filename(id)
    con = !self.file.attached? ? nil : self.file.filename
  end

  #Get File
  def get_file(id) 
    id = !self.file.attached? ? nil : self.file.id
    filename = !self.file.attached? ? nil : self.file.filename  
    file = !self.file.attached? ? nil : rails_blob_path(self.file,only_path:true)
    results = {
      id: id,
      file: file,
      filename: filename
    }
  end
  
  #Get File Export
  def get_export_file(id)    
    exfile = !self.exportfile.attached? ? nil : rails_blob_path(self.exportfile,only_path:true)
  end

end
