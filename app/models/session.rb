class Session < ApplicationRecord
    belongs_to :user

    def self.new_login(params)
        params[:password] = Digest::SHA2.new(512).hexdigest(params[:password])
        @user = User.where("username = ? and password = ?",params[:username].downcase,params[:password]).first
        if @user
            token = Digest::SHA1.hexdigest([Time.now, rand].join)
            @session = Session.new(user_id: @user[:id], token: token)
            if @session.save
                return {user_info: @user, token: @session.token}
            else
                return {error: @session.errors}
            end
        else
            return {error: "Invalid Username or Password."}
        end
    end

end
