class FormItem < ApplicationRecord
  belongs_to :form
  belongs_to :data_type
  has_many :dropdowns
  has_many :user_form_items, dependent: :destroy
  validates :order_no, presence: true
  validates_presence_of :title
  validates_presence_of :data_type_id
  validates_uniqueness_of :order_no, scope: :form_id, on: :create
  def as_json(options = {})        
    super(:except => [:created_at,:updated_at]).merge!({
        dropdown: self.dropdowns.empty? ? nil : self.dropdowns.order("id ASC").as_json(:except => [:created_at,:updated_at]),
        data_type: self.data_type.type_value
    })
  end
  def self.new_form_item(item,form)
    @type_id = DataType.where("type_value = ?",item["data_type"]).first       
    if @type_id.id == 8 || @type_id.id == 10
      item[:av_search] = false
    end         
    @form_item = self.new(
      form_id: form.id,
      data_type_id: @type_id.id,
      order_no: item[:order_no],
      title: item[:title],
      description: item[:description],
      required: item[:required],
      av_search: item[:av_search]
    )
    if @form_item.save
      if @type_id.id == 9 || @type_id.id == 7
        item[:value].each do |drop|
          Dropdown.new_dropdown(drop,@form_item)
        end
      end
    else
      return {error: "Can't save form_item."}
    end    
  end
  def self.update_form_item(item,form)  
    # item can change order_no
    @type_id = DataType.where("type_value = ?",item[:data_type]).first          
    @form_item = self.where("id = ? and form_id = ?",item[:id],form[:id]).first
    if @type_id.id == 8 || @type_id.id == 10
      item[:av_search] = false
    end
    if @form_item
      # Update
      @form_item[:order_no] = item[:order_no]
      @form_item[:data_type_id] = @type_id.ref_index
      @form_item[:title] = item[:title]
      @form_item[:description] = item[:description]
      @form_item[:required] = item[:required]
      @form_item[:av_search] = item[:av_search]
      if @form_item.save
        if @type_id.ref_index == 9 || @type_id.ref_index == 7
          item[:value].each do |drop|
            if drop[:id]
              Dropdown.update_dropdown(drop,@form_item)
            else
              Dropdown.new_dropdown(drop,@form_item)
            end # end if drop[:id]
          end # end Loop item[:value]          
        end #end if ref_index
      else
        return {error: "Can't save form_item."}
      end # end if .save
    end
  end  

  def self.delete(params)
    if !(params[:user_id].blank?)
      @user = User.find(params[:user_id])
      
      params[:form_items].each do |fit|
        @form_item = FormItem.find(fit["id"])
        @form = Form.find(@form_item["form_id"])
        if @form.user_id == params[:user_id] || @user.role_id == 3
          @form_item.destroy
        else
          return {error: "Can not delete this form."}
        end
      end
      return {message: "Delete form_items Successfully."}
    else
      return {error: "Don't have user_id."}
    end
  end
end