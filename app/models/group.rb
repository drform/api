class Group < ApplicationRecord
    include Rails.application.routes.url_helpers
    belongs_to :user
    has_many :forms, dependent: :destroy
    has_one_attached :image
    validates_presence_of :title
    validates_presence_of :description
    validates_uniqueness_of :title

    def as_json(options = {})     
        super.merge!({
            image: !self.image.attached? ? nil : rails_blob_path(self.image,only_path:true)
        })           
    end  

    def self.search_group(params)
        user = User.find(params[:user_id])
        if user.username == "tester01" || user.username == "tester02" || user.username == "tester03" || user.username == "tester04"
            @group = Group.all.limit(params[:limit] ||= 6).offset(params[:offset] ||= 0).order(params[:order] ||= 'groups.updated_at desc')
            amount = (Group.all).count
            if !(params[:keyword].blank?)
                @group = @group.where("title ~* ? or description ~* ?", params[:keyword], params[:keyword])
            end
            return {
                amount: amount,
                groups: @group
            }
        else
            @group = Group.where("id != 2").limit(params[:limit] ||= 6).offset(params[:offset] ||= 0).order(params[:order] ||= 'groups.updated_at desc')
            amount = (Group.where("id != 2")).count
            if !(params[:keyword].blank?)
                @group = @group.where("title ~* ? or description ~* ?", params[:keyword], params[:keyword])
            end
            return {
                amount: amount,
                groups: @group
            }
        end
        # @group = Group.all.limit(params[:limit] ||= 6).offset(params[:offset] ||= 0).order(params[:order] ||= 'groups.updated_at desc')
        # amount = (Group.all).count
        # if !(params[:keyword].blank?)
        #     begin
        #         date = params[:keyword].to_date
        #         @group = @group.where("to_char(created_at, 'YYYY-MM-DD') ~* ?", date)
        #     rescue
        #         @group = @group.where("title ~* ? or description ~* ?", params[:keyword], params[:keyword])
        #     end
        # end
        # # return {
        # #     amount: amount,
        # #     groups: @group
        # # }

        # return @group
    end

    def self.new_group(params, user_id)
        chkuser = User.find(user_id)
        if chkuser.role_id == 3
            if !(params[:group][:image].blank?)
                begin
                    params[:group][:image] = Group.decode_64(params[:group][:image])
                    @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S")
                    @group = Group.new(user_id: params[:group][:user_id], title: params[:group][:title], description: params[:group][:description])
                    if @group.save
                        @group.image.attach(io:File.open(params[:group][:image]), filename: @filename)
                        return { message: "Save group successfully.",
                                group_info: @group}
                    else
                        return { error: @group.errors }
                    end
                rescue
                    @group = Group.new(user_id: params[:group][:user_id], title: params[:group][:title], description: params[:group][:description], image: params[:group][:image])
                    if @group.save
                        return { message: "Save group successfully.",
                                group_info: @group}
                    else
                        return { error: @group.errors }
                    end  
                end
            else
                @group = Group.new(user_id: params[:group][:user_id], title: params[:group][:title], description: params[:group][:description])
                    if @group.save
                        @group.image.attach(io: File.open('./folder.png'), filename: 'folder.png', content_type: 'image/png')
                        return { message: "Save group successfully.",
                                group_info: @group}
                    else
                        return { error: @group.errors }
                    end  
            end
        else
            return {error: "You're not admin."}
        end
    end

    def self.update_group(params, user_id)
        @group = Group.find(params[:id])
        chkuser = User.find(user_id)
        if chkuser.role_id == 3
            if !(params[:group][:image].blank?)
                begin
                    params[:group][:image] = User.decode_64(params[:group][:image])
                    @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S")
                    if  @group.update(user_id: params[:group][:user_id], title: params[:group][:title], description: params[:group][:description])
                        @group.image.attach(io:File.open(params[:group][:image]), filename: @filename)
                        return { message: "Update group successfully.",
                                group_info: @group}
                    else
                        return { error: @group.errors }
                    end
                rescue
                    if @group.update(user_id: params[:group][:user_id], title: params[:group][:title], description: params[:group][:description], image: params[:group][:image])
                        return { message: "Update group successfully.",
                                group_info: @group}
                    else
                        return { error: @group.errors }
                    end  
                end
            else
                if @group.update(user_id: params[:group][:user_id], title: params[:group][:title], description: params[:group][:description])
                    return { message: "Update group successfully.",
                            group_info: @group}
                else
                    return { error: @group.errors }
                end 
            end
        else
            return {error: "You're not admin."}
        end
    end

    def self.delete(params,user_id)
        @group = Group.find(params[:id])
        chkuser = User.find(user_id)
        if chkuser.role_id == 3
            @group.destroy
            return {message: "Delete Successfully",
                data: @group}
        else
            return {error: "You're not admin."}
        end
    end
end
