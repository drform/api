class SharesController < ApplicationController
  before_action :set_share, only: [:show, :update]
  before_action :set_user, only: [:destroy]

  # GET /shares
  def index
    @shares = Share.get_share(params)

    render json: @shares
  end

  # GET /shares/1
  def show
    render json: @share
  end

  # POST /shares
  def create
    params[:user_id] = @user_session.user_id
    @share = Share.new_share(params)
    render json: @share
  end

  # PATCH/PUT /shares/1
  def update
    if @share.update(share_params)
      render json: @share
    else
      render json: @share.errors, status: :unprocessable_entity
    end
  end

  # DELETE /shares/1
  def destroy
    params[:user_id] = @user_session.user_id
    @share = Share.delete(params)
    render json: @share
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_share
      @share = Share.find(params[:id])
    end

    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def share_params
      params.require(:share).permit(:username, :form_id)
    end
end
