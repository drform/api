class UsersController < ApplicationController
  skip_before_action :set_session, :only => [:create]
  before_action :set_user, only: [:show, :destroy]

  # GET /users
  def index
    @users = User.search_user(params, @user_session.user_id)
    render json: @users
  end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    @user = User.new_register(user_params)
    render json: @user
  end

  # PATCH/PUT /users/1
  def update
    @user = User.update(params, @user_session.user_id)
    render json: @user
  end

  # DELETE /users/1
  def destroy
    params[:user_id] = @user_session.user_id
    @user = User.delete(params)
    render json: @user
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:username, :password, :name, :email, :tel, :position, :role_id, :image)
    end
end
