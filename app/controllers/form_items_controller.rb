class FormItemsController < ApplicationController
  before_action :set_form_item, only: [:show, :update]

  # GET /form_items
  def index
    @form_items = FormItem.all

    render json: @form_items
  end

  # GET /form_items/1
  def show
    render json: { item:@form_item.order("order_no ASC")}
  end

  # POST /form_items
  def create
    @form_item = FormItem.new(form_item_params)

    if @form_item.save
      render json: @form_item, status: :created, location: @form_item
    else
      render json: @form_item.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /form_items/1
  def update
    if @form_item.update(form_item_params)
      render json: @form_item
    else
      render json: @form_item.errors, status: :unprocessable_entity
    end
  end

  # DELETE /form_items
  def destroy
    params[:form_items] = JSON.parse(params[:form_items]) if params[:form_items].class == String  
    params[:user_id] = @user_session.user_id
    @form_item = FormItem.delete(params)
    render json: @form_item
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_form_item
      @form_item = FormItem.where("form_id = ?",params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def form_item_params
      params.require(:form_item).permit(:form_id, :data_type_id, :title, :description, :required, :av_search)
    end
end
