class ShareRolePermissionsController < ApplicationController
  before_action :set_share_role_permission, only: [:show, :update, :destroy]

  # GET /share_role_permissions
  def index
    @share_role_permissions = ShareRolePermission.all

    render json: @share_role_permissions
  end

  # GET /share_role_permissions/1
  def show
    render json: @share_role_permission
  end

  # POST /share_role_permissions
  def create
    @share_role_permission = ShareRolePermission.new(share_role_permission_params)

    if @share_role_permission.save
      render json: @share_role_permission, status: :created, location: @share_role_permission
    else
      render json: @share_role_permission.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /share_role_permissions/1
  def update
    if @share_role_permission.update(share_role_permission_params)
      render json: @share_role_permission
    else
      render json: @share_role_permission.errors, status: :unprocessable_entity
    end
  end

  # DELETE /share_role_permissions/1
  def destroy
    @share_role_permission.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_share_role_permission
      @share_role_permission = ShareRolePermission.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def share_role_permission_params
      params.require(:share_role_permission).permit(:share_role_id, :permission_description)
    end
end
