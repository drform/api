class UserFormItemsController < ApplicationController
  before_action :set_user_form_item, only: [:show, :update, :destroy]

  # GET /user_form_items
  def index
    @user_form_item = UserFormItem.all    
    render json: @user_form_items
  end

  # GET /user_form_items/1
  def show    
    render json: @user_form_item
  end

  # POST /user_form_items
  def create
    @user_form_item = UserFormItem.search(params) 
    render json: @user_form_item
  end

  def export
    exfile = UserFormItem.export_file(params,@user_session.user_id)
    render json: exfile
  end

  # PATCH/PUT /user_form_items/1
  def update
    if @user_form_item.update(user_form_item_params)
      render json: @user_form_item
    else
      render json: @user_form_item.errors, status: :unprocessable_entity
    end
  end

  # DELETE /user_form_items/1
  def destroy
    @user_form_item.destroy
  end

  
  def destroyImage
    params[:images] = JSON.parse(params[:images]) if params[:images].class == String  
    params[:user_id] = @user_session.user_id
    @user_form_item = UserFormItem.delete_image(params)
    render json: @user_form_item
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_form_item
      @user_form_item = UserFormItem.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_form_item_params
      params.require(:user_form_item).permit(:user_form_id, :form_item_id, :date_value, :datetime_value, :string_value, :text_value, :integer_value, :float_value, :checkbox_value, :file_value, :dropdown_value_id)
    end
end
