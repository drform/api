class UserFormsController < ApplicationController
  before_action :set_user_form, only: [:show]
  
  # GET /user_forms
  def index  
    @user_form= UserForm.get_user_form(params)
    render json: @user_form
  end

  def search 
    @user_forms = UserForm.search(params)
    render json: @user_forms
  end
  
  # GET /user_forms/1
  def show        
    @user_form = UserForm.get_Data(params)
    render json: @user_form
  end

  # POST /user_forms
  def create        
    params[:user_form_item] = JSON.parse(params[:user_form_item]) if params[:user_form_item].class == String  
    @user_form = UserForm.new_user_form(params,@user_session.user_id) 
    render json: @user_form 
  end

  # PATCH/PUT /user_forms/1
  def update    
    @user_form = UserForm.user_answer_update(params,@user_session.user_id)
    render json: @user_form
  end

  # DELETE /user_forms/1
  def destroy
    params[:user_id] = @user_session.user_id
    @user_form = UserForm.delete(params)
    render json: @user_form
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_form
      @user_form = UserForm.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_form_params
      params.require(:user_form).permit(:user_id, :form_id)
    end
end
