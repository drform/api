class DropdownsController < ApplicationController
  before_action :set_dropdown, only: [:show, :update, :destroy]

  # GET /dropdowns
  def index
    @dropdowns = Dropdown.all

    render json: @dropdowns
  end

  # GET /dropdowns/1
  def show
    render json: @dropdown
  end

  # POST /dropdowns
  def create
    @dropdown = Dropdown.new(dropdown_params)

    if @dropdown.save
      render json: @dropdown, status: :created, location: @dropdown
    else
      render json: @dropdown.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /dropdowns/1
  def update
    if @dropdown.update(dropdown_params)
      render json: @dropdown
    else
      render json: @dropdown.errors, status: :unprocessable_entity
    end
  end

  # DELETE /dropdowns/1
  def destroy
    @dropdown.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dropdown
      @dropdown = Dropdown.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def dropdown_params
      params.require(:dropdown).permit(:form_item_id, :dropdown_value)
    end
end
