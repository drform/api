class GroupsController < ApplicationController
  before_action :set_group, only: [:show, :update, :destroy]

  # GET /groups
  def index
    params[:user_id] = @user_session.user_id
    @groups = Group.search_group(params)
    render json: @groups
  end

  # GET /groups/1
  def show
    render json: @group
  end

  # POST /groups
  def create
    params[:group] = JSON.parse(params[:group]) if params[:group].class == String   
    params[:group][:user_id] = @user_session.user_id
    @group = Group.new_group(params,@user_session.user_id)
    render json: @group
  end

  # PATCH/PUT /groups/1
  def update
    params[:group] = JSON.parse(params[:group]) if params[:group].class == String   
    params[:group][:user_id] = @user_session.user_id
    @group = Group.update_group(params,@user_session.user_id)
    render json: @group
  end

  # DELETE /groups/1
  def destroy
    @group = Group.delete(params,@user_session.user_id)
    render json: @group
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group
      @group = Group.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def group_params
      params.require(:group).permit(:user_id, :title, :description, :image)
    end
end
