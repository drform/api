class PasswordsController < ApplicationController
    skip_before_action :set_session, :only => [:forgot, :reset]
    def forgot
      @user = User.forgot(params)
      render json: @user
    end
    
    def reset
      @user = User.reset(params)
      render json: @user
    end
end
