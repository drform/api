class ApplicationController < ActionController::API
    before_action :set_session
  def set_session
    if request.headers["token"].nil?
      render json: {message: "Unknown token."},status: :unauthorized
    else
      @user_session = Session.find_by_token(request.headers["token"])  
      if @user_session.nil?
        render json: {message: "Unknown token."},status: :unauthorized
      end
    end    
  end
end
