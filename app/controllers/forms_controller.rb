class FormsController < ApplicationController
  before_action :set_form, only: [:show, :update, :destroy]

  # GET /forms
  def index        
    params[:user_id] = @user_session.user_id
    @forms = Form.search_form(params)
    render json: @forms
  end

  # GET /forms/1
  def show    
    @form = Form.get_Datas(@form,@user_session.user_id,params)
    render json: @form
  end

  # POST /forms
  def create
    params[:form_item] = JSON.parse(params[:form_item]) if params[:form_item].class == String    
    @form = Form.new_form(params,@user_session.user_id)
    render json: @form
  end #end create

  # PATCH/PUT /forms/1
  def update 
    params[:form_item] = JSON.parse(params[:form_item]) if params[:form_item].class == String
    @form_updates = Form.form_update(params,@user_session.user_id)
    render json: @form_updates
  end

  # DELETE /forms/1
  def destroy
    params[:user_id] = @user_session.user_id
    @form = Form.delete(params)
    render json: @form
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_form
      @form = Form.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def form_params

      params.require(:form).permit(:user_id, :title, :description)
    end
end
